function toggleSidebar() {
    var sidebar = document.getElementById('sidebar');
    var menuBtn = document.getElementById('menu-btn');
    if (sidebar.style.left === '-200px') {
        sidebar.style.left = '0';
        menuBtn.style.left = '210px';
    } else {
        sidebar.style.left = '-200px';
        menuBtn.style.left = '10px';
    }
} 